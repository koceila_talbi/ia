/*********************************************
 * Modèle pour le problème du carré latin
 *********************************************/
using CP;

/*** Données du problème  ***/
int n=10;
 
 
 
/*** Variables et domaines  ***/
range d=1..n;

dvar int valeurs[d][d] in d;

/*** affichage seulement de 1ere solution ***/
main {
	thisOplModel.generate();
	if(cp.solve()){
	var obj=cp.getObjValue()
	thisOplModel.postProcess();
	}
}

/*** Contraintes  ***/
constraints{
	/*entiers d'une même ligne 
	et de colonnes differentes sont differents */
	forall(i,j,k in d : j < k) {
		
		valeurs[i][j] != valeurs[i][k];
		
	}
	/*entiers d'une même colonne
	et de lignes differentes sont differents */ 
	forall(i,j,k in d : i < k) {
		
		valeurs[i][j] != valeurs[k][j];
		
	}
	
}





/*** Post-traitement  ***/
execute {
/*
	for(i in d)
	{		
		for(j in d ){
		writeln(valeurs[i][j]);
		}
	}
*/

writeln("       ",valeurs);

}

