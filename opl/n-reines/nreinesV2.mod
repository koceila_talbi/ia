/*********************************************
 * Modèle pour le problème des n-reines
 *
 * Les données de l'instance sont décrites dans un fichier .dat externe
 *********************************************/
using CP;

//----- Parametrage Solveur -----




//----- Données du problème -----
range n= 1..5;



 
//----- Variables et domaines -----
range r=1..8;


dvar int Reinesx[n] in r;
dvar int Reinesy[n] in r;


//----- Contraintes -----
constraints{

forall(i,j in n : i < j) {
		
	Reinesx[i] != Reinesx[j];
		
	}
forall(i,j in n : i < j) {
		
	Reinesy[i] != Reinesy[j];
		
	}

forall(i,j in n : i < j) {
		
	abs(Reinesx[i] - Reinesx[j]) != abs(Reinesy[j]-Reinesy[i]);
		
	}

}

//Matrice d'affichage
string mat[r][r];
//----- Post-traitement -----

execute {


for(var i in r){
	for(var j in r){
		mat[i][j]="-";
	}
}


for(var i in n){
		mat[Reinesx[i]][Reinesy[i]]="R";
}


writeln("       ",mat);

}


