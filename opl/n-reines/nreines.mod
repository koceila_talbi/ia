/*********************************************
 * Modèle pour le problème des n-reines
 *
 * Les données de l'instance sont décrites dans un fichier .dat externe
 *********************************************/
using CP;

//----- Parametrage Solveur -----


//----- Données du problème -----
range n= 1..8;

range r=1..8; 
//----- Variables et domaines -----

dvar int y[n] in r;

//----- Contraintes -----
constraints{

forall(i,j in n : i < j) {
		
	y[i] != y[j];
		
	}

forall(i,j in n : i < j) {
		
	abs(y[j] - y[i]) != abs(i-j);
		
	}

}

//Matrice d'affichage
string mat[r][r];
//----- Post-traitement -----

execute {

	for(var i in r){
		for(var j in r){
			mat[i][j]="-";
		}
	}


	for(var i in n){
		mat[i][y[i]]="R";
	}

	writeln("       ",mat);
}


