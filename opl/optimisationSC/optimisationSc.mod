/*********************************************
 * Modèle pour le problème d'optimisation
 *********************************************/
using CP;

/*** Données du problème  ***/

{int} p = {1, 2, 5, 10, 20, 50} ;

range n = 1..99;

/*** Variables et domaines  ***/

dvar int lesPieces[n][p] in n ;


/*** Fonction objective  ***/

minimize sum (i in p) lesPieces[n][i] ;


/*** Contraintes  ***/

subject to {

	forall(i in n ,j in p : i < j) {	
		lesPieces[i][j]*j < i ;

	}
 
}

execute {
writeln("nombre de pièces ",lesPieces);
	}


/*** Post-traitement  ***/

