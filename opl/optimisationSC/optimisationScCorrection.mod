unsing cp;

{int} valpieces = {1,2,5,10,20,50};
int sommeMaxi = 99;
//------------------------------------

range valeur = 1..sommeMaxi;
dvar int nbPices[val in valPieces] in 0..(sommeMaxi div val):
dvar int nbPieces[val in valPices, v in valeurs] in 0..(v div val);
dvar int nbTotalDePices in 0..(sum(val in valPieces) sommeMaximale div val;

minimize 
	nbTotalPieces;
subject to{
	nbTotalDePieces == sum(val in valPieces) nbPieces[val];

	forall(v in valeurs){
		v==sum(val in valPieces) v*nbPiecesVal[val,v];
		forall(val in valPieces){
			nbPiecesVal[val,v] <= nbPieces[val];
		}
	}
