/*********************************************
 * Modèle OPL Pour le problème des billes
 *
 * Utiliser un (ou plusieurs) tableau(x) afin de regrouper les variables 
 * dans des structures adéquates.
 * Cela doit permettre de simplifier l'énoncé de certaines contraintes 
 * ainsi que l'affichage de la solution.
 *********************************************/
using CP;			/* Utilisation du solveur CP-Solver */

/* Déclarations domaines et variables */

range d = 4..7;

{string} personnes = {"Anne","Bernard","Claudine","Denis"};
{string} couleurs = {"Jaune","Bleu","Rouge","Noir"};
{string} lieux = {"Jardin","Parc","Chambre","Salon"};

{string} indexVars = personnes union couleurs union lieux ;

dvar int varDec[indexVars] in d;


/* Paramétrage du solveur */
execute {
	cp.param.searchType="DepthFirst";
	cp.param.workers=1;
}

/*Block de control de flux*/
include "allSolutions.mod";



/* Contraintes */

constraints{


 //Les variables de décision sont differrentes 4 à 4 


	forall(i,j in personnes : i < j) {
		
		varDec[i] != varDec[j];
	}
	

	forall(i,j in couleurs : i < j) {
		
		varDec[i] != varDec[j];
	}

	forall(i,j in lieux : i < j) {
		
		varDec[i] != varDec[j];
	}



   //Denis joue dans le parc et n’a pas 4 ans, contrairement à
   //l’enfant qui a des billes bleues.
	varDec["Denis"] == varDec["Parc"];
	varDec["Denis"] != 4;
	varDec["Bleu"] != varDec["Parc"];
	varDec["Bleu"] == 4;
   
   //2. La fille de 6 ans a des billes jaunes.
	 varDec["Jaune"] == 6;
	 varDec["Bernard"] != 6;
	 varDec["Denis"] != 6;
   
   //3. L’enfant qui joue avec des billes noires est plus âgé que l’enfant qui joue 
   //dans le jardin mais plus jeune que Anne.
	varDec["Noir"] > varDec["Jardin"];
	varDec["Noir"] < varDec["Anne"];
   
   //4. Anne, qui joue dans sa chambre, a 1 an de plus que l’enfant qui joue dans le salon.
	varDec["Anne"] == varDec["Chambre"];
	varDec["Anne"] == varDec["Salon"] + 1;
  


}
/* Post-traitement (Affichage Solution) */


execute {
	for(i in personnes)
	{		
		writeln("Age de ",i," = ",varDec[i]," ans");
		for(j in couleurs)
		{
			if (varDec[i] == varDec[j])
			writeln("joue avec la bille ",j);
		}
		for(k in lieux)
		{
			if (varDec[i] == varDec[k])
			writeln("lieu de jeu : ",k);
		}
		
		writeln("-----------------------------");
		
	}


}

