/*********************************************
 * Modèle OPL Pour le problème des billes
 *
 * Dans ce modèle, chaque variable doit être déclarée explicitement
 * avec son nom d'origine dans le modèle initial
 *
 *********************************************/
using CP;			/* Utilisation du solveur CP-Solver */

/* Déclarations domaines et variables */
range d=4..7;

dvar int Anne in d;
dvar int Bernard in d;
dvar int Claudine in d;
dvar int Denis in d;
dvar int Bleu in d;
dvar int Jaune in d;
dvar int Noir in d;
dvar int Rouge in d;
dvar int Jardin in d; 
dvar int Parc in d;
dvar int Chambre in d;
dvar int Salon in d;

//{int} d = {4,5,6,7};



/* Paramétrage du solveur */
execute {
	cp.param.searchType="DepthFirst";
	cp.param.workers=1;
}

main {
	thisOplModel.generate();
	if(cp.solve()){
	var obj=cp.getObjValue()
	thisOplModel.postProcess();
	}
}

/* Contraintes */ 

constraints {
   //Denis joue dans le parc et n’a pas 4 ans, contrairement à
   //l’enfant qui a des billes bleues.
	Denis == Parc;
	Denis != 4;
	Bleu != Parc;
	Bleu == 4;
   
   //2. La fille de 6 ans a des billes jaunes.
	 Jaune == 6;
	 Bernard != 6;
	 Denis != 6;
   
   //3. L’enfant qui joue avec des billes noires est plus âgé que l’enfant qui joue 
   //dans le jardin mais plus jeune que Anne.
	Noir > Jardin;
	Noir < Anne;
   
   //4. Anne, qui joue dans sa chambre, a 1 an de plus que l’enfant qui joue dans le salon.
	Anne == Chambre;
	Anne > Salon + 1;

	Anne != Bernard;
	Anne != Claudine;
	Anne != Denis;

	Bernard != Claudine;
	Bernard != Denis;
	
	Claudine != Denis;
	
	Jaune  != Rouge;
	Jaune != Noir;
	Rouge != Noir;	
	Noir != Bleu;
	Bleu != Jaune;
	Bleu != Noir;
	Bleu != Rouge;
	

	Jardin != Parc;
	Jardin != Chambre;
	Jardin != Salon;
	Parc != Chambre;
	Chambre != Salon;
}

/* Post-traitement (Affichage Solution) */ 

execute {
	writeln("Age de Anne = ", Anne);
	writeln("Age de Bernard = ", Bernard);
	writeln("Age de Claudine = ", Claudine);
	writeln("Age de Denis = ", Denis);
}
